# Dockerfile Raspberry Pi Nginx
FROM resin/rpi-raspbian:jessie

RUN apt-get update && \
	apt-get install nginx \
	php5-fpm \
	php5-curl \
	php5 \
    php5-json \
    php5-gd \
    php5-sqlite \
    php5-common \
	supervisor

RUN mkdir -p /etc/nginx/sites-available /etc/nginx/sites-enabled /var/www

# COPY PHP-FPM Configuration
#COPY ./nginx/conf.d/php5-fpm.conf /etc/nginx/conf.d/php5-fpm.conf

RUN sed -e 's/;cgi.fix_pathinfo=1/cgi.fix_pathinfo=0/' </etc/php5/fpm/php.ini >~/php.ini.tmp && \
     mv ~/php.ini.tmp /etc/php5/fpm/php.ini

# COPY Supervisor
COPY ./etc/supervisord.conf /etc/

VOLUME ["/etc/nginx/sites-enabled", "/var/log/nginx"]

# Boot up Nginx, and PHP5-FPM when container is started
CMD /usr/bin/supervisord --nodaemon --configuration /etc/supervisord.conf
#CMD [/bin/bash]